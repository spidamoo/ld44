

let   game_scale = 1;

let global_container;
let map_container;
let objects_on_map_layer;
let viewport_width;
let viewport_height;

let left_gui;
let right_gui;
let rates_texts = {};
const rates_stats = ['amount', 'rate', 'value'];
let gameover_text;
let game_time;
let hoard_sprite;
let game_time_text;
let total_value_text;

const start_hoard       = 3;
const start_decay_speed = 0.2;
const loot_amount       = '1000..2000';
const plunder_damage    = 0.09;
const trade_bonus       = 0.03;
const caravan_cooldown  = '8 .. 12';

function init_game() {
    global_container      = new PIXI.Container();
    map_container         = new PIXI.Container();
    objects_on_map_layer  = new PIXI.display.Layer( new PIXI.display.Group(0, true) );

    apply_scale();

    app.stage.addChild(global_container);

    global_container.addChild(map_container);
    global_container.addChild(objects_on_map_layer);

    init_map();
    init_rates();
    init_hoard();

    left_gui = create_sprite('gui:left');
    left_gui.y = viewport_height;
    left_gui.anchor.set(0, 1);
    global_container.addChild(left_gui);

    right_gui = create_sprite('gui:right');
    right_gui.y = viewport_height;
    right_gui.x = viewport_width;
    right_gui.anchor.set(1, 1);
    global_container.addChild(right_gui);


    rates_texts = {};
    for (const i in currencies) {
        const cur = currencies[i];
        rates_texts[cur] = {};
        for (const j in rates_stats) {
            const stat = rates_stats[j];
            rates_texts[cur][stat] = new PIXI.Text(
                '',
                new PIXI.TextStyle({
                    fontFamily: 'spidamoo',
                    fontSize: 17,
                    fill: '#666666',
                })
            );
            rates_texts[cur][stat].anchor.set(0, 0.5);
            rates_texts[cur][stat].x = 35 + 35 * j;
            rates_texts[cur][stat].y = viewport_height - 80 + i * 31;
            global_container.addChild(rates_texts[cur][stat]);
        }
    }

    game_time = 0;
    gameover_text = new PIXI.Text(
        '',
        new PIXI.TextStyle({
            fontFamily: 'spidamoo',
            fontSize: 32,
            fill: '#000000',
            align: 'center',
        })
    );
    gameover_text.anchor.set(0.5, 0.5);
    gameover_text.position.set(viewport_width * 0.5, viewport_height * 0.5 + 100);
    gameover_text.visible = false;
    global_container.addChild(gameover_text);

    hoard_sprite = create_sprite('gui:hoard');
    hoard_sprite.stop();
    hoard_sprite.position.set(viewport_width - 90, viewport_height - 30);
    global_container.addChild(hoard_sprite);

    total_value_text = new PIXI.Text(
        '',
        new PIXI.TextStyle({
            fontFamily: 'spidamoo',
            fontSize: 18,
            fill: '#666666',
        })
    );
    total_value_text.anchor.set(0.5, 0.5);
    total_value_text.position.set(viewport_width - 24, viewport_height - 265);
    global_container.addChild(total_value_text);

    game_time_text = new PIXI.Text(
        '',
        new PIXI.TextStyle({
            fontFamily: 'spidamoo',
            fontSize: 18,
            fill: '#666666',
        })
    );
    game_time_text.anchor.set(0.5, 0.5);
    game_time_text.position.set(viewport_width - 24, viewport_height - 210);
    global_container.addChild(game_time_text);
}

function start_game(level) {
    $('.screen').hide();
    $('#pause_icon').show();

    _init.done(function() {
        console.log('init game');
        init_game();
        apply_window_size();
        show_game();

        app.ticker.start();
    });
}

function finish_game() {
    app.ticker.stop();

    app.stage.removeChildren();

    $('#pause_icon').hide();
}

function game_over() {
    finish_game();
    show_game_over();
}

function leave_game() {
    finish_game();
    show_main_menu();
}

function apply_scale() {
    global_container.scale.x = game_scale;
    global_container.scale.y = game_scale;
    apply_window_size();
}

function update_game(dt) {
    let center_x = 0;
    let center_y = 0;

    objects_on_map_layer.x = (viewport_width  * 0.5 - center_x);
    objects_on_map_layer.y = (viewport_height * 0.5 - center_y);

    for (const object of map_objects) {
        object.update(dt);
    }
    for (const object of dynamic_objects) {
        object.update(dt);
    }
    dynamic_objects = dynamic_objects.filter(function(_) {return !_.destroyed});

    update_rates(dt);
    update_hoard(dt);

    for (const cur of currencies) {
        rates_texts[cur].amount.text    = hoard[cur].toFixed(1) + 'k';
        rates_texts[cur].rate.text      = rates[cur].toFixed(2);
        rates_texts[cur].value.text     = money_value(hoard[cur], cur).toFixed(2);
    }

    if (the_dragon.dead) {
        gameover_text.visible = true;
        gameover_text.text = `You lived for ${game_time.toFixed(0)} years before Demon of Greed took you.\nEverybody in the land lived long and prospered ever since.`;
    }
    else {
        game_time += dt;
    }

    if (total_hoard < 1) {
        hoard_sprite.gotoAndStop(0);
    }
    else if (total_hoard < 5) {
        hoard_sprite.gotoAndStop(1);
    }
    else if (total_hoard < 10) {
        hoard_sprite.gotoAndStop(2);
    }
    else {
        hoard_sprite.gotoAndStop(3);
    }

    game_time_text.text   = game_time.toFixed(0);
    total_value_text.text = (total_value / decay_speed).toFixed(1);
}


function roll(expr) {
    if (expr === undefined) {
        return undefined;
    }
    if (typeof expr == typeof true) {
        return expr;
    }
    if (typeof expr == typeof 1) {
        return expr;
    }

    if ( params = expr.match(/^(\d+?)\s*\.\.\s*(\d+?)$/) ) {
        const from = parseInt(params[1]);
        const to   = parseInt(params[2]);
        const result = from + Math.floor( Math.random() * (1 + to - from) );
        return result;
    }

    return expr;
}
function choose_random_item(array) {
    return array[Math.floor(Math.random() * array.length)];
}

function vector_length(x, y) {
    return Math.sqrt(x*x + y*y);
}
function distance_between(x1, y1, x2, y2) {
    return vector_length(x1 - x2, y1 - y2);
}

function create_sprite(name, anim) {
    let graphics;
    if (PIXI.loader.resources[name].spritesheet) {
        if (anim !== undefined) {
            if (PIXI.loader.resources[name].spritesheet.animations[anim]) {
                return _create_anim(name, anim);
            }
            else if (PIXI.loader.resources[name].spritesheet.textures[anim]) {
                return _create_sprite(name, anim);
            }
            else if (PIXI.loader.resources[name].spritesheet.textures[anim + '1']) {
                return _create_sprite(name, anim + '1');
            }
        }
        if (Object.keys(PIXI.loader.resources[name].spritesheet.animations).length) {
            const first_anim =  Object.keys(PIXI.loader.resources[name].spritesheet.animations)[0];
            return _create_anim(name, first_anim);
        }
        else {
            const first_texture =  Object.keys(PIXI.loader.resources[name].spritesheet.textures)[0];
            return _create_sprite(name, first_texture);
        }
    }
    else {
        graphics = new PIXI.Sprite(PIXI.loader.resources[name].texture);
        graphics.anchor.set(0.5, 0.5);
        return graphics;
    }
}

function _create_anim(name, anim) {
    const graphics = new PIXI.extras.AnimatedSprite(PIXI.loader.resources[name].spritesheet.animations[anim]);
    graphics.updateAnchor = true;
    graphics.animationSpeed = 0.02;
    graphics.play();
    return graphics;
}
function _create_sprite(name, texture) {
    return new PIXI.Sprite(PIXI.loader.resources[name].spritesheet.textures[texture]);
}

let showing_stats_of;
let $stats;
function show_stats(object) {
    showing_stats_of = object;
    // for (const attr in object) {
        
    // }
    $stats.show();
}
function hide_stats() {
    $('#stats').hide();
    showing_stats_of = undefined;
}
function update_stats(object) {
    $stats.html( JSON.stringify(
        object,
        function(key, val) {
            if ( typeof(val) == typeof(1) ) {
                return val.toFixed(2);
            }
            return val;
        },
        1
    ) );
}
