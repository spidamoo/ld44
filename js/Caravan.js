class Caravan extends DynamicObject {
    constructor(from, to) {
        const x = from.x;
        const y = from.y;

        super(x, y, 'caravan');

        this.from   = from;
        this.to     = to;

        this.x = x;
        this.y = y;

        this.speed = 40;

        this.set_target(this.to.x, this.to.y);
    }

    update(dt) {
        super.update(dt);

        if (this.distance_to_target < 40) {
            this.graphics.alpha = 0.025 * this.distance_to_target;
        }
        if (this.lifetime < 1) {
            this.graphics.alpha = this.lifetime;
        }
    }

    finish() {
        rates[this.to.own_currency] += trade_bonus;
        this.to.notifications.push( new Notification(this.x, this.y, 'rate_up', '') );
        this.destroy();
    }

    plunder() {
        this.destroy();
    }
}
