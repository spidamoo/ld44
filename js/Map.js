
let   map_objects       = [];
let   dynamic_objects   = [];

function init_map() {
    map_objects = [];

    the_lair = new Lair(0, 20);

    add_map_object(the_lair);

    add_map_object( new Flag(-208, -120, 1, 'crown') );
    add_map_object( new Flag(208, -120, 1, 'escudo') );
    add_map_object( new Flag(0, 240, 1, 'taler') );

    add_map_object( new Town(-208, -120, 1, 'crown') );
    add_map_object( new Town(208, -120, 1, 'escudo') );
    add_map_object( new Town(0, 240, 1, 'taler') );

    dynamic_objects = [];

    the_dragon = new Dragon(the_lair.x, the_lair.y);

    add_dynamic_object(the_dragon);
}

function add_dynamic_object(object) {
    object.draw();
    dynamic_objects.push(object);
}

function add_map_object(object) {
    object.draw();
    map_objects.push(object);
}

class MapObject {
    constructor(x, y, type) {
        this.x = x;
        this.y = y;

        this.type = type;

        this.notifications = [];
        this.notification_cooldown = 0;
    }

    draw() {
        this.graphics = create_sprite(this.type);
        this.graphics.zIndex = this.y;
        this.put_graphics();
    }
    put_graphics() {
        this.graphics.x = this.x;
        this.graphics.y = this.y;
        objects_on_map_layer.addChild(this.graphics);
    }

    update(dt) {
        this.graphics.zIndex = this.y;

        if (this.notifications.length && this.notification_cooldown < 0) {
            add_dynamic_object( this.notifications.shift() );
            this.notification_cooldown = 0.5;
        }
        this.notification_cooldown -= dt;
    }
}

class DynamicObject {
    constructor(x, y, type) {
        this.x = x;
        this.y = y;

        this.type = type;

        this.lifetime = 0;
    }

    draw() {
        this.graphics = create_sprite(this.type);
        this.put_graphics();

        const self = this;
        // this.graphics.interactive = true;
        // this.graphics.on('pointerover', function() {
        // });
        // this.graphics.on('pointerout', function() {
        // });
    }
    put_graphics() {
        this.graphics.x = this.x;
        this.graphics.y = this.y;
        objects_on_map_layer.addChild(this.graphics);
    }

    update(dt) {
        if (this.target_x !== undefined && this.target_y !== undefined) {
            this.distance_to_target = distance_between(this.x, this.y, this.target_x, this.target_y);
            if (this.distance_to_target < this.speed * dt) {
                this.x = this.target_x;
                this.y = this.target_y;
                this.finish();
                return;
            }
            else {
                this.x += this.dx * dt;
                this.y += this.dy * dt;
            }
        }

        this.lifetime += dt;

        this.graphics.x = this.x;
        this.graphics.y = this.y;
        if (this.dx < -0.1) {
            this.graphics.scale.x = -1;
        }
        else {
            this.graphics.scale.x =  1;
        }
        this.graphics.zIndex = this.y;
    }

    set_target(x, y) {
        this.target_x = x;
        this.target_y = y;

        this.angle = Math.atan2(this.target_y - this.y, this.target_x - this.x);
        this.dx = Math.cos(this.angle) * this.speed;
        this.dy = Math.sin(this.angle) * this.speed;
    }
    reset_target() {
        this.target_x = this.target_y = undefined;
    }
    has_target() {
        return this.target_x !== undefined && this.target_y !== undefined;
    }

    destroy() {
        this.graphics.destroy();
        this.destroyed = true;
    }
}

class Notification extends DynamicObject {
    constructor(x, y, type, message) {
        super(x, y, 'notification');

        this.speed = 100;
        this.set_target(x, y - 200);

        this.notification_type = type;
        this.message           = message;
    }

    draw() {
        this.graphics = new PIXI.Container();
        this.icon = create_sprite('icon:' + this.notification_type);
        this.graphics.addChild(this.icon);

        if (this.message) {
            this.icon.anchor.set(1, 0.5);
            this.text = new PIXI.Text(
                this.message,
                new PIXI.TextStyle({
                    fontFamily: 'spidamoo',
                    fontSize: 28,
                    fill: '#666666',
                })
            );
            this.text.anchor.set(0, 0.5);
            this.graphics.addChild(this.text);
        }

        this.put_graphics();
    }

    update(dt) {
        super.update(dt);
        // console.log(this);
        this.graphics.zIndex = 3000;
        if (this.distance_to_target < 100) {
            this.graphics.alpha = 0.01 * this.distance_to_target;
        }
    }

    finish() {
        this.destroy();
    }
}

