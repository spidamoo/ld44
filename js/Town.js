const currencies = ['crown', 'escudo', 'taler'];
const rates = {};

function init_rates() {
    for (const cur of currencies) {
        rates[cur] = 1;
    }
}

function update_rates() {
    for (const cur of currencies) {
        if (rates[cur] < 0.001) {
            rates[cur] = 0.001;
        }
    }
}

class Town extends MapObject {
    constructor(x, y, tier, own_currency) {
        super(x, y, 'town');

        this.tier   = tier || 1;
        this.own_currency = own_currency || 'crown';

        this.caravan_cooldown = roll('0..3');

        this.burning_cooldown = 0;
    }

    draw() {
        this.graphics = new PIXI.Container();
        this.intact_anim = create_sprite('town', 'intact');
        this.burning_anim = create_sprite('town', 'burning');
        this.burning_anim.visible = false;

        this.graphics.addChild(this.intact_anim);
        this.graphics.addChild(this.burning_anim);

        const self = this;
        this.graphics.on('pointerdown', function() {
            the_dragon.go_plunder(self);
        });

        this.graphics.interactive = true;
        this.graphics.on('pointerover', function() {
        });
        this.graphics.on('pointerout', function() {
        });

        this.put_graphics();
    }

    update(dt) {
        if (this.caravan_cooldown <= 0) {
            this.send_caravan();
        }
        else {
            this.caravan_cooldown -= dt;
        }

        if (this.burning_anim.visible) {
            if (this.burning_cooldown > 0) {
                this.burning_cooldown -= dt;
            }
            else {
                this.rebuild();
            }
        }

        super.update(dt);
    }

    send_caravan() {
        const self = this;
        const target = choose_random_item( map_objects.filter(function(_) {return _.type == 'town' && _ != self}) );

        if (!target) {
            return;
        }

        const caravan = new Caravan(this, target);
        add_dynamic_object(caravan);

        this.reset_caravan_cooldown();
    }

    reset_caravan_cooldown() {
        this.caravan_cooldown = roll(caravan_cooldown);
    }

    plunder() {
        if (rates[this.own_currency] <= plunder_damage) {
            // return;
        }
        rates[this.own_currency] -= plunder_damage;
        the_dragon.money[this.own_currency] += roll(loot_amount) / 1000;

        this.burn();

        this.notifications.push( new Notification(this.x, this.y, 'rate_down', '') );
    }

    burn() {
        this.burning_anim.visible = true;
        this.burning_anim.gotoAndPlay(0);
        this.intact_anim.visible = false;
        this.burning_cooldown = 5;
    }
    rebuild() {
        this.burning_anim.visible = false;
        this.intact_anim.visible = true;
    }
}

function money_value(money, currency) {
    return money * rates[currency];
}

function money_for(value, currency) {
    return value / rates[currency];
}

function give_money(to, money) {
    for (const cur in money) {
        to.money[cur] += money[cur];
    }
}
function take_money(from, money) {
    for (const cur in money) {
        from.money[cur] -= money[cur];
    }
}

class Flag extends MapObject {
    constructor(x, y, tier, own_currency) {
        super(x, y - 20, 'flag');

        this.own_currency = own_currency || 'crown';
    }

    draw() {
        this.graphics = create_sprite('flags', this.own_currency);
        this.put_graphics();
    }
}
