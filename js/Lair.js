const hoard = {};
const hoard_exposed = {};
let   decay_speed;
let   total_hoard;
let   total_value;

function init_hoard() {
    decay_speed = start_decay_speed;
    for (const cur of currencies) {
        hoard[cur] = start_hoard;
        hoard_exposed[cur] = true;
    }
}

function update_hoard(dt) {
    const currencies_to_take = currencies.filter(function(_) {return hoard_exposed[_] && hoard[_] > 0});

    if (currencies_to_take.length) {
        const share = 1 / currencies_to_take.length;
        for (const cur of currencies_to_take) {
            hoard[cur] -= money_for(dt * decay_speed * share, cur);
            if (hoard[cur] < 0) {
                hoard[cur] = 0;
            }
        }
    }
    else {
        the_dragon.kill();
    }

    total_hoard = 0;
    total_value = 0;
    for (const cur of currencies) {
        total_hoard += hoard[cur];
        total_value += money_value(hoard[cur], cur);
    }
}


let the_lair;
class Lair extends MapObject {
    constructor(x, y) {
        super(x, y, 'lair');
    }
}

let the_dragon;
class Dragon extends DynamicObject {
    constructor(x, y) {
        super(x, y, 'dragon');

        this.speed = 150;

        this.money = {};
        for (const cur of currencies) {
            this.money[cur] = 0;
        }

        this.current_state = 'idle';
    }

    draw() {
        this.graphics = new PIXI.Container();
        this.idle_anim = create_sprite('dragon', 'idle');
        this.fly_anim = create_sprite('dragon', 'fly');
        this.fly_anim.stop();
        this.fly_anim.visible = false;
        this.attack_anim = create_sprite('dragon', 'attack');
        this.attack_anim.stop();
        this.attack_anim.visible = false;

        this.graphics.addChild(this.idle_anim);
        this.graphics.addChild(this.fly_anim);
        this.graphics.addChild(this.attack_anim);

        this.graphics.zIndex = 2000;

        this.put_graphics();
    }

    go_plunder(town) {
        if (this.dead) {
            return;
        }
        this.target_for_plunder = town;
        const shift_x = this.target_for_plunder.x > this.x ? -200 : 200;
        this.set_state('fly');
        this.set_target(this.target_for_plunder.x + shift_x, this.target_for_plunder.y - 100);
    }

    set_state(state) {
        if (state == this.current_state) {
            return;
        }
        this.idle_anim.visible = false;
        this.fly_anim.visible = false;
        this.fly_anim.stop();
        this.attack_anim.visible = false;
        this.attack_anim.stop();

        this.current_state = state;

        switch (state) {
            case 'fly':
                this.fly_anim.visible = true;
                this.fly_anim.gotoAndPlay(0);
                this.speed = 200;
            break;
            case 'idle':
                this.current_state = 'idle';
                this.idle_anim.visible = true;
            break;
            case 'attack':
                this.current_state = 'attack';
                this.attack_anim.visible = true;
                this.attack_anim.gotoAndPlay(0);
                this.speed = 90;
            break;
        }
    }

    update(dt) {
        super.update(dt);

        this.graphics.zIndex = 2000;
    }

    finish() {
        if (this.target_for_plunder && this.current_state == 'fly') {
            this.set_state('attack');
            this.set_target(this.target_for_plunder.x, this.target_for_plunder.y - 100);
        }
        else if (this.target_for_plunder && this.current_state == 'attack') {
            this.target_for_plunder.plunder();
            this.target_for_plunder = undefined;
            this.set_state('fly');
            this.set_target(the_lair.x, the_lair.y);
        }
        else {
            for (const cur of currencies) {
                if (this.money[cur] == 0) {
                    continue;
                }

                the_lair.notifications.push( new Notification( the_lair.x, the_lair.y, cur, '+' + (this.money[cur] * 1000).toFixed(0) ) );

                hoard[cur] += this.money[cur];
                this.money[cur] = 0;
            }
            this.reset_target();
            this.set_state('idle');
        }
    }

    kill(dt) {
        this.dead = true;
    }
}
