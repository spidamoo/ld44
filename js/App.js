const search_params = window.location.search.substring(1).split('&').reduce( (obj, p) => {const [n, v] = p.split('='); obj[n] = v === undefined ? true : v; return obj}, {} );
console.log('params', search_params);
const DEBUG_DRAW = search_params.debug;

const settings = {
    sound           : true,
    music           : true,
    scale           : 1,
};

$(function() {
    init_app();

    $('button').filter(function() {return $(this).data('target')}).click(function() {
        show( $(this).data('target') );
    });
    $('button.start_game').click(function() {
        start_game(search_params.level);
    });
    $('button.back').click(function() {
        show('main_menu');
    });

    $.get('version.txt', function(data) {
        $('#version').text('version: ' + data);
        console.log('version', data);
    });
});

let app;
let window_w;
let window_h;
let view_x;
let view_y;
let _init = $.Deferred();

function apply_window_size() {
    window_w = $(window).width()  - 0;
    window_h = $(window).height() - 5;

    app.renderer.autoResize = true;

    app.renderer.resize( Math.min(window_w, 800), Math.min(window_h, 600) );

    viewport_width  = app.renderer.width  / game_scale;
    viewport_height = app.renderer.height / game_scale;

    view_x = $(app.view).offset().left;
    view_y = $(app.view).offset().top;

    console.log('apply_window_size', viewport_width, viewport_height);
}

const sound_list = {
};
const music_list = {
};
function update_sound_setting() {
    if (settings.sound) {
        $('#sound_icon').removeClass('off');
        Object.keys(sound_list).forEach(s => PIXI.sound.find(s).muted = false);
    }
    else {
        $('#sound_icon').addClass('off');
        Object.keys(sound_list).forEach(s => PIXI.sound.find(s).muted = true);
    }
}
function update_music_setting() {
    if (settings.music) {
        $('#music_icon').removeClass('off');
        Object.keys(music_list).forEach(s => PIXI.sound.find(s).muted = false);
    }
    else {
        $('#music_icon').addClass('off');
        Object.keys(music_list).forEach(s => PIXI.sound.find(s).muted = true);
    }
}

function load_settings() {
    try {
        const stored_settings = JSON.parse( window.localStorage.getItem('hoarder_settings') );

        if (stored_settings) {
            for (const setting in settings) {
                if (stored_settings[setting] !== undefined) {
                    settings[setting] = stored_settings[setting];
                }
            }
        }

        if (!stored_settings || !stored_settings.uuid) {
            save_settings();
        }
    } catch (e) {}

    update_sound_setting();
    update_music_setting();
}
function save_settings() {
    window.localStorage.setItem( 'hoarder_settings', JSON.stringify(settings) );
}

function init_app() {
    $stats = $('#stats');
    app = new PIXI.Application();

    app.renderer.backgroundColor = 0xF8F8F8;

    $(window).resize(function() {
        apply_window_size();
    });

    $('#renderer').append(app.view);

    // PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    PIXI.loader
        .add('lair', 'img/map_object/lair.json')
        .add('town', 'img/map_object/town.json')
        .add('caravan', 'img/dynamic_object/caravan.json')
        .add('dragon', 'img/dynamic_object/dragon.json')

        .add('flags', 'img/map_object/flags.json')

        .add('icon:crown', 'img/crown_small.png')
        .add('icon:escudo', 'img/escudo_small.png')
        .add('icon:taler', 'img/taler_small.png')
        .add('icon:rate_up', 'img/rate_up.png')
        .add('icon:rate_down', 'img/rate_down.png')

        .add('gui:left', 'img/gui_left.png')
        .add('gui:right', 'img/gui_right.png')
        .add('gui:hoard', 'img/hoard.json')
    ;

    Object.keys(sound_list).forEach( s => PIXI.loader.add(s, sound_list[s]) );
    Object.keys(music_list).forEach( s => PIXI.loader.add(s, music_list[s]) );

    PIXI.loader.onProgress.add((e) => {update_loader_box(e.progress)});

    app.stage = new PIXI.display.Stage();
    // app.stage.interactive = true;
    app.stage.group.enableSort = true;

    PIXI.loader.load( (loader, resources) => {
        load_settings();
        show_main_menu();
        // start_music('intro');
        console.log('loaded resources', resources, PIXI.sound);
        _init.resolve();
    } );

    app.ticker.add( () => {
        const dt = 0.017;//app.ticker.elapsedMS * 0.001;
        update_game(dt);
    } );
    app.ticker.stop();
}

function show_main_menu() {
    show('main_menu');
}

function show_game_over() {
    show('game_over');
    submit_and_show_records( $('#game_over .leaderboards') );
}

function show_game() {
    show('game');
    $('#ui_td').show();
    $('#final_td').hide();
}

function show(what) {
    $('.screen').hide();
    $('#' + what).show();
}

function show_loader_box() {
    $('.screen').hide();
    $('#loading').show();
}


